FROM python:3.6.5-slim-jessie

WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y --no-install-recommends \
		build-essential \
	&& rm -rf /var/lib/apt/lists/*

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 8881:8881

CMD [ "python", "/usr/src/app/app.py" ]
