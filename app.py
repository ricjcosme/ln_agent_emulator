# -*- coding: utf-8 -*-

import bottle
import canister
import json
from bottle import response, request
from pyld import jsonld
from multiprocessing import Pool
from tinydb import TinyDB, where
import time, datetime
import ed25519
import base58
import os
import requests
import copy

bottle.BaseRequest.MEMFILE_MAX = 1024 * 1024

t = 5
whoami = "did:v1:test:nym:3SFX4qWxdXzRrPZ6KUvELFqyHbcvPrD4QknV61vjbmV7"
signing_key = ed25519.SigningKey(base58.b58decode(b"5ZVdkyhngCaBXte4Lu6nad4dVkveVkhLA52hkjjkNSjDxs3SG4CKqenptyaV9ZYjqudorTFK4bDFrEbqPeFE8W9"))
verifying_key = ed25519.VerifyingKey(base58.b58decode(whoami.split(":")[4]))

app = bottle.Bottle()
app.install(canister.Canister())


"""" This would be an expected request
{
  iss: 'did:foo:123abc',
  aud: 'did:bar:456def',
  '@type': 'Actions/Add',
  request: {
    schema: 'schema.org/AppendAction'
  },
  payload: {
    meta: {
      title: 'Please verify this credential',
      tags: ['credential', 'verify']
    },
    data: {
      "@context": ['http://schema.org/', 'https://dominode.com/schema/'],
      "@type": "RequestAttestationAction",
      "name": "DriversLicense",
      "object": CREDENTIAL_JSON-LD
    }
  }
}
"""

"""" This would be an expected response for a verified credential
An additional signature will be added to the signature array on VERIFIED_CREDENTIAL_JSON-LD 
{
  iss: 'did:bar:456def',
  aud: 'did:foo:123abc',
  '@type': 'Actions/Response',
  payload: [
    VERIFIED_CREDENTIAL_JSON-LD
  ],
  meta: {
    actionStatus: 'CompletedActionStatus',
    result: 'Verified'    
  }
}
"""

"""" This would be an expected response for a rejected credential
No additional signature is added to INITIAL_CREDENTIAL_JSON-LD
{
  iss: 'did:bar:456def',
  aud: 'did:foo:123abc',
  '@type': 'Actions/Response',
  payload: [
    INITIAL_CREDENTIAL_JSON-LD
  ],
  meta: {
    actionStatus: 'CompletedActionStatus',
    result: 'Rejected'    
  }
}
"""


def verify_identity(did, cred):
    """
    Let's get the credential issuer value (a DID), resolve it via universal resolver or Dominode's resolver
    and fetch its public key. Next we're going to identify which of the signatures inside the signature array
    property matches the issuer. Last we're going to validate the credential JSON-LD with the public key we got.
    If it does not validate we return a http 400 and ignore the request.
    """
    try:
        cred_signatures = cred["signature"]
        for sig in cred_signatures:
            if sig["creator"].split("#")[0] == did:
                r = requests.get(''.join(['http://did-resolver.dominode.com/ddo/', did]))
                ddo = r.json()
                public_key = ddo["authentication"][0]["publicKey"][0]["publicKeyBase58"]
                signature_value = sig["signatureValue"]

                json_credential = copy.deepcopy(cred)
                del json_credential["signature"]
                jsonld.set_document_loader(jsonld.requests_document_loader(timeout=5))
                normalized = jsonld.normalize(
                    json_credential, {'algorithm': 'URDNA2015', 'format': 'application/n-quads'})
                verify_key = ed25519.VerifyingKey(base58.b58decode(public_key))
                verify_key.verify(sig=signature_value,
                                        msg=normalized.encode("utf-8"),
                                        encoding="base64")
                return None, 200
    except Exception as e:
        app.log.info(str(e))
        if str(e) == "Bad Signature":
            return str(e), 400
        else:
            return str(e), 500


def sign(doc):
    """ Request format evaluation """
    try:
        time.sleep(t)
        ts = int(time.time())
        if doc["doc"]["claim"]["givenName"].upper() != 'EDUARDO':
            sig_array = doc["doc"]["signature"]
            del doc["doc"]["signature"]
            normalized = jsonld.normalize(
                doc["doc"], {'algorithm': 'URDNA2015', 'format': 'application/n-quads'})
            sig = signing_key.sign(normalized.encode('utf-8'), encoding="base64")
            signature_data = {
                "@type": "Ed25519Signature2018",
                "created": datetime.datetime.now().isoformat(),
                "creator": whoami + "#authn-key-1",
                "signatureValue": sig.decode('ascii'),
                "meta" : {
                    "result": "Verified", # or Rejected
                    "instrument": {
                        "@type": "Product",
                        "name": "LexisNexis® Instant Verify"
                    }
                }
            }
            sig_array.append(signature_data)
            doc["doc"]["signature"] = sig_array
            db.update({'doc': doc["doc"], 'state': "Verified", 'updated': ts}, where('id') == doc["doc"]["id"])
        else:
            db.update({'doc': doc["doc"], 'state': "Rejected", 'updated': ts}, where('id') == doc["doc"]["id"])

    except Exception as e:
        info = "Work on signing execution error: %s" % str(e)
        app.log.info(info)


@app.post('/.identity/action')
def action():

    """" Base request definition. A request for
    action - RequestAttestationAction - to be more
    precise
    """

    ts = int(time.time())

    """ Request obj format parsing """
    try:
        req = json.loads(request.body.read())
        req_iss = req["iss"]
        signed_cred = req["payload"]["data"]["object"]
        cred_id = signed_cred["id"]
        cred_issuer = signed_cred["issuer"]
        res, status_code = verify_identity(cred_issuer, signed_cred)
        if status_code != 200:
            response.status = status_code
            return json.dumps({"message": res})

        info = "Received request for verification of credential with id: %s - Request body: %s" % (cred_id, \
                                                                                                   json.dumps(req))
        app.log.info(info)

        headers = request.headers
        if "X-Correlation-Id" not in headers:
            app.log.info("No correlation ID found")
        else:
            correlation_id = headers.get("X-Correlation-Id")
            app.log.info("Correlation ID: %s" % correlation_id)

    except Exception as e:
        info = "Malformed request: %s" % str(e)
        app.log.info(info)
        response.status = 400
        return json.dumps({"message": info})

    """ Work on credential verification """
    try:
        if db.contains(where('id') == cred_id):
            if db.contains((where('id') == cred_id) & (where('state') == "Verified")) or \
                    db.contains((where('id') == cred_id) & (where('state') == "Rejected")):
                response.status = 200
                r = db.search(where('id') == cred_id)
                resp = {
                        "iss": whoami,
                        "aud": r[0]["doc"]["id"],
                        '@type': 'Actions/Response',
                        "payload": [
                          r[0]["doc"]
                        ],
                        "meta": {
                            "actionStatus": "CompletedActionStatus",
                            "result": r[0]["state"]
                        }
                    }
                info = "Response body: %s" % json.dumps(resp)
                app.log.info(info)

                return json.dumps(resp)
            else:
                response.status = 202
                return None
        else:
            cred_obj = {"id": cred_id,
                        "cid": correlation_id,
                        "issuer": cred_issuer,
                        "doc": signed_cred,
                        "created": ts,
                        "state": "OPEN"}
            db.insert(cred_obj)
            p = pool.Process(target=sign, args=(cred_obj,))
            p.start()
            response.status = 202
            return None

    except Exception as e:
        info = "Work on credential execution error: %s" % str(e)
        app.log.info(info)
        response.status = 500
        return json.dumps({"message": info})



""" let's start """
if __name__ == "__main__":
    if os.path.exists('/tmp/db.json'):
        os.remove('/tmp/db.json')
    db = TinyDB('/tmp/db.json')
    pool = Pool(processes=2)
    app.run(host='0.0.0.0', port=8881)
